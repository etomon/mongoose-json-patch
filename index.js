/*
 * Mongoose JSON Patch Plugin
 *
 * Don't patch like an idiot
 * http://williamdurand.fr/2014/02/14/please-do-not-patch-like-an-idiot/
 */

var _ = require('lodash'),
	mpath = require('mpath'),
	jsonpatch = require('fast-json-patch');

/*
 * Configuration
 * TODO: make configurable
 */
var globalReadBlacklist = [];
var globalWriteBlacklist = ['_id', '__v'];

/*
 * Plugin
 */
module.exports = exports = function checkPermissions(schema, options) {

	var self = {};

	// ...................
	// === PATCH LOGIC ===
	// ^^^^^^^^^^^^^^^^^^^

	//Find all attributes that have writable set to false
	var schemaWriteBlacklist = _.filter(Object.keys(schema.paths), function(pathName) {
		var path = schema.path(pathName);
		if(path.options && path.options.writable !== undefined) {
			return path.options.writable === false;
		}
		return false;
	});

	//Add them to the blacklist
	self.writeBlacklist = _.union(globalWriteBlacklist, schemaWriteBlacklist);

	function patchMethod (patches, callback){
		var i;

		if (!Array.isArray(patches))
			return callback(new Error(`No patches were provided`));

		//Check to make sure none of the paths are on the write blacklist
		for(i = 0; i < self.writeBlacklist.length; i++) {
			var pathName = mpathToJSONPointer(self.writeBlacklist[i]);

			for(var j = 0; j < patches.length; j++) {
				if(patches[j].path == pathName) {
					return callback(new Error('Modifying ' + pathName + ' is not allowed.'));
				}
			}
		}

		//Apply the patch
		try {

			// Make sure all tests pass
			// TODO: This can be removed once JSON-Patch #64 is fixed
			// https://github.com/Starcounter-Jack/JSON-Patch/issues/64
			for (i = 0; i < patches.length; i++) {
				var patch = patches[i];
				if(patch.op == 'test') {
					var success = jsonpatch.applyOperation(this, patch, true);
					if(!success) {
						return callback(new Error('The json-patch test op at index [' + i + '] has failed. No changes have been applied to the document.'));
					}
				}
			}

			// Dirty fix for mongoose-json-patch #3
			// https://github.com/winduptoy/mongoose-json-patch/issues/3
			for (i = 0; i < patches.length; ) {
				var p = patches[i];

				let path = p.path.substring(1).replace(/\//g, '.');

				if (p.op === 'remove') {
					let index = path.substr(path.length - 1, 1);
					let arr = index && !isNaN(index) && _.get(this, path.split('.').slice(0, -1).join('.'));

					if (arr && arr.isMongooseArray) {
						arr.splice(Number(index) - 1, 1);
					} else {
						let obj = _.get(this, path);
						if (obj && obj.remove) {
							obj.remove();
						}
						_.set(this, path,  undefined);
					}
					_.pullAt(patches, i);
				} else if (p.op === 'replace' || p.op === 'add') {
					let blocks = path.split('.');

					let pull = false;
					for (let z = 0; z < blocks.length; z++) {
						let b = blocks[z];
						if (!isNaN(b)) {
							let possibleArray = blocks.slice(0, z).join('.');
							if (!_.get(this, possibleArray)) {
								_.set(this, possibleArray, []);
							}
							let remainingPath = blocks.slice(z+1);

							if (_.get(this, possibleArray).set) {
								let existingRoot = _.get(this, possibleArray)[Number(b)];
								if (existingRoot) {
									if (remainingPath && remainingPath.length) {
										let key = remainingPath.slice(0, -1).join('.');
										if (_.isEmpty(key) && !_.isEmpty(_.get(remainingPath, '0'))) {
											key = remainingPath[0];
										}
										if (Array.isArray(_.get(existingRoot, key))) {
											let arr = Array.from(_.get(existingRoot, key)).map(p => (p.toObject && p.toObject({virtuals: true})) || p);
											let newI = Number(remainingPath.slice(-1)[0]);
											arr[newI] = p.value;
											_.remove(existingRoot, key);
											_.set(existingRoot, key, arr.slice(0));
										} else {
											_.set(existingRoot, key, p.value);
										}
									} else {
										_.get(this, possibleArray)[Number(b)] = p.value;
									}
								} else {
									let newval = (p.value);
									if (remainingPath.length) {
										newval = {};
										_.set(newval, remainingPath.join('.'), p.value);
									}
									_.get(this, possibleArray).set(Number(b), newval);
								}
							}
							else {
								let newval = (p.value);
								if (remainingPath.length) {
									newval = {};
									_.set(newval, remainingPath.join('.'), p.value);
								}
								_.get(this, possibleArray)[Number(b)] = newval;
							}
							pull = true;
							continue;
						}


						let bp = blocks.slice(0, z+1).join('.');
						let bz = blocks.slice(z+1).join('.');
						let o = _.get(this, bp);
						if (o && o._doc) {
							_.set(this, blocks.join('.'), p.value);
							pull = true;
							break;
						}
					}
					if (pull)
						_.pullAt(patches, i);
					else
						i++;
				} else {
					i++;
				}
			}

			jsonpatch.applyPatch(this, patches, true);
		} catch(err) {
			return callback(err);
		}

		callback(null);
	};


	schema.method('patchValidate', function (patches, callback) {
		var self = this;
		patchMethod.call(this, patches, function (err, results) {
			if (err) {
				callback(err);
				return;
			}

			if(self.ownerDocument && self.ownerDocument())
				return self.ownerDocument().validate(callback);
			return self.validate(callback);
		});
	});


	schema.method('patch', function (patches, callback) {
		var self = this;
		patchMethod.call(this, patches, function (err, results) {
			if (err) {
				callback(err);
				return;
			}

			if(self.ownerDocument && self.ownerDocument()) return self.ownerDocument().save(callback);
			return self.save(callback);
		});
	});

	/*
	 * Basic implementation converter Mongoose/MongoDB style paths to JSON Pointers (RFC6901)
	 * user.authLocal.email -> /user/authLocal/email
	 */
	function mpathToJSONPointer(path) {
		return '/' + path.split('.').join('/');
	}

	// ................................
	// === PROTECTED PROPERTY LOGIC ===
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

	//Find all attributes that have readable set to false
	var schemaReadBlacklist = _.filter(Object.keys(schema.paths), function(pathName) {
		var path = schema.path(pathName);
		if(path.options && path.options.readable !== undefined) {
			return path.options.readable === false;
		}
		return false;
	});

	self.readBlacklist = _.union(globalReadBlacklist, schemaReadBlacklist);

	/*
	 * Takes this object and removes any properties that are marked as {readable: false} in the schema.
	 * Supplying any additional keys as arguments will remove them.
	 */
	schema.method('filterProtected', function() {

		var thisObj = this.toObject({virtuals: true, getters: true});

		var readBlacklist = _.union(self.readBlacklist, arguments);
		for(var i = 0; i < readBlacklist.length; i++) {
			var pathName = readBlacklist[i];
			mpath.set(pathName, undefined, thisObj);
		}

		return thisObj;
	});

	//Static method for calling filterProtected on an array of documents
	schema.statics.filterProtected = function(collection) {

		var args = [].slice.call(arguments); //Copy the arguments
		args.shift(); //Remove the first argument (collection)

		for(var i = 0; i < collection.length; i++) {
			collection[i] = collection[i].filterProtected.apply(collection[i], args);
		}
		return collection;
	};

};
